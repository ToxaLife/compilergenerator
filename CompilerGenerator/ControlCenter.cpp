//
// Created by Александр Поповкин on 29/11/15.
//

#include "ControlCenter.hpp"
#include "generator/SlrGenerator.hpp"
#include "parser/SlrParser.hpp"

#include "flex/flex.h"
#include "bison/bison.tab.hpp"

using namespace std;

void ControlCenter::MainFunc(char *grammar, std::string grammar_name, bool zero_iteration) {
    long env[26];
    yyscan_t scanner;
    struct Extra extra;
    Nterm* root = nullptr;

    if (zero_iteration) {
        //Bison
        root = new Nterm(S);
        init_scanner(grammar, &scanner, &extra, true);

        int bison_result = yyparse(scanner, env, root);
        if (bison_result)
            throw invalid_argument("Bison error:Wrong Grammar.");
    } else {
        //SLR
        YYTOKEN *yytoken;
        vector<YYTOKEN> tokens;
        init_scanner(grammar, &scanner, &extra, false);
        do {
            yytoken = new YYTOKEN();
            yytoken->value.tag = yylex(&(yytoken->value), &(yytoken->coords), scanner);
            tokens.push_back(*yytoken);

        } while (yytoken->value.tag != 0);

        try {
            SlrParser *slr_parser = new SlrParser(tokens);
            root = slr_parser->Parse();
        } catch (exception& e) {
            cerr << e.what() << endl;
            throw invalid_argument("SLR Parser error detected");
        }
    }
//    root->to_string(0);
    AstTree* ast;
    try {
        ast = new AstTree(*root);
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw invalid_argument("AST error detected.");
    }
    try {
        SlrGenerator *slr_table = new SlrGenerator(ast, grammar_name);
        slr_table->GenerateSlrTable();
        slr_table->WriteGrammarToFile();
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw invalid_argument("SLR Generator error detected");
    }
}