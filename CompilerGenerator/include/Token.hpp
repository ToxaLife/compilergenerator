
#include <iostream>
#pragma once
#include "../symbol/Term.hpp"
#include "../symbol/Nterm.hpp"


typedef struct _YYSTYPE {
    int tag;
    char* value;
    int length;
    Term *term;
    Nterm *nterm;

    char* get_value() {
        char* token_value = (char*)calloc((static_cast<unsigned int>(length + 1)), sizeof(char));
        for (unsigned int i = 0; i < length; ++i) {
            token_value[i] = value[i];
        }
        return token_value;
    }
} YYSTYPE;

typedef struct _YYLTYPE {
    int first_line;
    int first_column;
    int last_line;
    int last_column;
    std::string to_string() {
        return "(" + std::to_string(first_line) + "," + std::to_string(first_column) + " - " + std::to_string(last_line) + "," + std::to_string(last_column) + ")";
    }
} YYLTYPE;

typedef struct _YYTOKEN {
    YYSTYPE value;
    YYLTYPE coords;
    void toString() {
        std::cout << "Token name: " << TermDeclNames[value.tag] << " Coords: " << coords.to_string() << " value :" << value.get_value() << std::endl;
    }
} YYTOKEN;
