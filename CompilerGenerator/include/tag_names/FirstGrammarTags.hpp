//
// Created by Alexander Popovkin on 10/02/16.
//

#ifndef COMPILERGENERATOR_TAGNAMES_HPP
#define COMPILERGENERATOR_TAGNAMES_HPP


enum NtermDecl {
    S = 1,
    A,
    T,
    N,
    R,
    Nlist,
    Tlist,
    Rrule,
    RightPath,
    Action,
    Chain,
    Sym,
    NLlist,
    NL,
};

const char* const NtermDeclNames[] = {
        "S'",
        "S",
        "A",
        "T",
        "N",
        "R",
        "Nlist",
        "Tlist",
        "Rrule",
        "RightPath",
        "Action",
        "Chain",
        "Sym",
        "NLlist",
        "NL"
};

enum TermDecl {
    NEW_LINE = 1,
    NNAME,
    TNAME,
    EQUAL,
    AXIOM_DECL,
    NTERM_DECL,
    TERM_DECL,
    RULE_DECL,
    EPS_DECL
};

const char* const TermDeclNames[] = {
        "",
        "<new_line>",
        "<Nname>",
        "<Tname>",
        "<equal>",
        "$AXIOM",
        "$NTERM",
        "$TERM",
        "$RULE",
        "$EPS"
};


#endif //COMPILERGENERATOR_TAGNAMES_HPP
