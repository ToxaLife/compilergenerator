//
// Created by Alexander Popovkin on 13/02/16.
//

#ifndef COMPILERGENERATOR_INPUTGRAMMARTAGS_HPP
#define COMPILERGENERATOR_INPUTGRAMMARTAGS_HPP



enum NtermDecl {
    E = 1,
    T,
    F,
};

const char* const NtermDeclNames[] = {
        "E'",
        "E",
        "T",
        "F",
};

enum NamesTermDecl {
    PLUS = 1,
    MULTIPLY,
    LBRACKET,
    RBRACKET,
    N,
};

const char* const TermDeclNames[] = {
        "EOF",
        "PLUS",
        "MULPIPLY",
        "LBRACKET",
        "RBRACKET",
        "N",
};

#endif //COMPILERGENERATOR_INPUTGRAMMARTAGS_HPP
