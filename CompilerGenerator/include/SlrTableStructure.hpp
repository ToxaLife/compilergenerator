//
// Created by Alexander Popovkin on 14/02/16.
//

#ifndef COMPILERGENERATOR_SLRTABLESTRUCTURE_HPP
#define COMPILERGENERATOR_SLRTABLESTRUCTURE_HPP

enum FieldState {
    err = 1,
    accept,
    shift,
    reduce,
};

const char* const FieldStateNames[] = {
        "reserved",
        "err",
        "accept",
        "shift",
        "reduce",
};



typedef struct _Field {
    FieldState state;
    unsigned int state_number;
    unsigned int reduce_rule;
    unsigned long rule_length;
    std::string to_string() {
        return "{" + static_cast<std::string>(FieldStateNames[state]) + ", " + std::to_string(state_number) +", " + std::to_string(reduce_rule) + ", "  + std::to_string(rule_length) + "}";
    }
} Field;

#endif //COMPILERGENERATOR_SLRTABLESTRUCTURE_HPP
