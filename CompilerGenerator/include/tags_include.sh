#!/usr/bin/env bash


HEADER=./include/IncludeTags.hpp
echo "#ifndef COMPILERGENERATOR_INCLUDETAGS_H" > $HEADER
echo "#define COMPILERGENERATOR_INCLUDETAGS_H" >> $HEADER
echo "" >> $HEADER

echo "#include \"tag_names/$1Tags.hpp\"" >> $HEADER

echo "" >> $HEADER
echo "#endif" >> $HEADER

echo "Header Tag file generated"
