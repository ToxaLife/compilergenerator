//
// Created by Alexander Popovkin on 02/02/16.
//

#ifndef COMPILERGENERATOR_SLRTABLE_H
#define COMPILERGENERATOR_SLRTABLE_H

#include <unordered_set>
#include "../ast_tree/AstTree.hpp"
#include "../symbol/Term.hpp"
#include "../include/SlrTableStructure.hpp"


typedef struct _Item {
    Term rule_name;
    TermChain rule_body;
    unsigned int position;


    bool operator==(const _Item& other) const {
        return rule_name == other.rule_name and position == other.position and rule_body == other.rule_body;
    }
} Item;



typedef std::unordered_set<const Term*> Sequence;
typedef std::vector<Item> State;

class SlrGenerator {

public:
    SlrGenerator(AstTree* ast, const std::string grammar_name);
    void GenerateSlrTable();
    void printStates();
    void printFirst();
    void printFollow();

    void WriteGrammarToFile();
private:

    AstTree* m_ast;
    const Term* m_axiom;
    const Term* m_epsilon;
    std::string m_grammar_name;

    std::vector<State> m_states;
    std::map<std::string, Sequence> m_first;
    std::map<std::string, Sequence> m_follow;

    std::vector<std::vector<Field>> m_action;
    std::vector<std::vector<int>> m_goto;


    void build_states();
    void find_first();
    void find_follow();
    void fill_slr_table();

    void slr_closure(State* state);
    State slr_goto(const State* state, const Term* symbol);


    int find_in_states(State state);
    Sequence find_first_for_seq(TermChain seq);

};


#endif //COMPILERGENERATOR_SLRTABLE_H
