//
// Created by Alexander Popovkin on 02/02/16.
//

#include <iostream>
#include "SlrGenerator.hpp"
#include <boost/range/irange.hpp>
#include <fstream>


using namespace std;

SlrGenerator::SlrGenerator(AstTree *ast, const string grammar_name) {
    m_ast = ast;

    if (not ast->ExtendTree()) {
        throw logic_error("AST Tree already extended");
    }
    m_axiom = ast->get_axiom();
    m_epsilon = ast->get_epsilon();
    assert(m_axiom);
    assert(m_epsilon);
    m_grammar_name = grammar_name;
}

void SlrGenerator::GenerateSlrTable() {
    build_states();
    find_first();
    find_follow();
    fill_slr_table();
}




void SlrGenerator::WriteGrammarToFile() {
    fstream slr_table_file;
    slr_table_file.open("slr_tables/" + m_grammar_name + ".hpp", fstream::out);



    slr_table_file << "#include <iostream>" << endl;
    slr_table_file << "\n\n\n";
    slr_table_file << "const Field action_table[" << m_action.size() << "][" << m_action[0].size() << "] = {" << endl;
    for (auto i_line : m_action) {
        slr_table_file << "\t{";
        for (auto i_field : i_line) {
            slr_table_file  << i_field.to_string() << ", ";
        }
        slr_table_file << "}," << endl;
    }
    slr_table_file << "};" << endl;
    slr_table_file << "\n\n";

    slr_table_file << "const int goto_table[" << m_goto.size() << "][" << m_goto[0].size() << "] = {" << endl;
    for (auto i_line : m_goto) {
        slr_table_file << "\t{";
        for (auto i_field : i_line) {
            slr_table_file << i_field << ", ";
        }
        slr_table_file << "}," << endl;
    }

    slr_table_file << "};" << endl;
    slr_table_file << "\n\n";

    slr_table_file.close();

}

void SlrGenerator::build_states(){
    try {
        State first_state;
        first_state.push_back({*m_axiom, m_ast->get_rule(m_axiom->get_name())[0], 0});
        slr_closure(&first_state);
        m_states.push_back(first_state);
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("BuildStates: error while building first state.");
    }

    bool added;
    do {
        added = false;

        for (unsigned int i = 0; i < m_states.size(); ++i) {
            try {
                auto i_state = m_states[i];
                for (auto i_term = m_ast->get_term_begin(); i_term < m_ast->get_term_end(); ++i_term) {
                    auto new_state = slr_goto(&i_state, &*i_term);

                    if (new_state.size() > 0 and find_in_states(new_state) <= 0) {
                        added = true;
                        m_states.push_back(new_state);
                    }
                }

                for (auto i_non_term = m_ast->get_non_term_begin();
                     i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
                    auto new_state = slr_goto(&i_state, &*i_non_term);

                    if (new_state.size() > 0 and find_in_states(new_state) <= 0) {
                        added = true;
                        m_states.push_back(new_state);
                    }
                }
            } catch(exception& e) {
                cerr << e.what() << endl;
                auto message = "BuildStates: error while building " + to_string(i) + " state.";
                throw logic_error(message);
            }

        }
    } while (added);

}

void SlrGenerator::slr_closure(State* state) {
    map<string, bool> added = {};

    for (unsigned int i = 0; i < state->size(); ++i ) {
        auto current_item = state->at(i);
        if (current_item.position >= current_item.rule_body.size()) continue;
        assert(current_item.position < current_item.rule_body.size());
        auto current_symbol = current_item.rule_body[current_item.position];
        if (current_symbol.get_tag() == NNAME and not added.count(current_symbol.get_name())) {
            for (auto i_rule_body : m_ast->get_rule(current_symbol.get_name())) {
                state->push_back({current_symbol, i_rule_body, 0});
            }
            added[current_symbol.get_name()] = true;
        }
    }
}

State SlrGenerator::slr_goto(const State* state, const Term* symbol) {
    State new_state = {};

    for (auto i_item : *state) {
        if (i_item.position < i_item.rule_body.size()) {
            auto current_symbol = i_item.rule_body[i_item.position];
            if (current_symbol == *symbol) {
                new_state.push_back(i_item);
                new_state.back().position++;
            }

        }
    }
    slr_closure(&new_state);

    return new_state;
}

void SlrGenerator::find_first() {

    try {
        for (auto i_term = m_ast->get_term_begin(); i_term < m_ast->get_term_end(); ++i_term) {
            m_first[i_term->get_name()] = {&*i_term};
        }

        for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
            m_first[i_non_term->get_name()] = {};
        }

    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("FIRST: error while initializing sets.");
    }

    bool added;
    do {
        added = false;
        for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
            try {
                for (auto i_rule_body: m_ast->get_rule(i_non_term->get_name())) {
                    if (find(i_rule_body.begin(), i_rule_body.end(), *m_epsilon) != i_rule_body.end()) {
                        m_first[i_non_term->get_name()].insert(m_epsilon);
                    } else {
                        bool eps_found;
                        unsigned int i = 0;
                        do {
                            eps_found = false;
                            if (not m_first.count(i_rule_body[i].get_name())) {
                                throw invalid_argument("Symbol from rule body not found in FIRST set.");
                            }
                            if (not m_first.count(i_non_term->get_name())) {
                                throw invalid_argument("Non terminal from rule body not found in FIRST set.");
                            }
                            for (auto first_term : m_first[i_rule_body[i].get_name()]) {
                                if (first_term == m_epsilon) {
                                    eps_found = true;
                                    continue;
                                }
                                auto report = m_first[i_non_term->get_name()].insert(first_term);
                                added |= report.second;
                            }
                            if (i == i_rule_body.size() - 1 and eps_found) {
                                auto report = m_first[i_non_term->get_name()].insert(m_epsilon);
                                added = true;
                            }
                            ++i;

                        } while (eps_found and i < i_rule_body.size());
                    }
                }
            } catch (exception& e) {
                cerr << e.what() << endl;
                throw logic_error("FIRST: error while filling first set for nonterminals");
            }

        }
    } while (added);

}
//
void SlrGenerator::find_follow() {

    m_follow[m_axiom->get_name()] = {m_epsilon};
    bool added;
    do {
        added = false;
        for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
            try {
                for (auto i_rule_body: m_ast->get_rule(i_non_term->get_name())) {
                    for (auto i : boost::irange(0, static_cast<int>(i_rule_body.size()))) {

                        if (i_rule_body[i].get_tag() == NNAME) {
                            auto nterm = i_rule_body[i];
                            TermChain beta = {};

                            for (auto j : boost::irange(min(i + 1, static_cast<int>(i_rule_body.size()) - 1),
                                                        static_cast<int>(i_rule_body.size()))) {
                                beta.push_back(i_rule_body[j]);

                            }

                            Sequence first_of_beta = find_first_for_seq(beta);

                            for (auto follow_term : first_of_beta) {
                                if (follow_term == m_epsilon) continue;
                                auto report = m_follow[nterm.get_name()].insert(follow_term);
                                added |= report.second;
                            }
                            if (not m_follow.count(nterm.get_name())) {
                                throw invalid_argument("Non terminal from rule body not found in FOLLOW set.");
                            }
                            if (i == i_rule_body.size() - 1 or first_of_beta.count(m_epsilon)) {
                                for (auto follow_term : m_follow[i_non_term->get_name()]) {
                                    auto report = m_follow[nterm.get_name()].insert(follow_term);
                                    added |= report.second;
                                }
                            }


                        }
                    }
                }
            } catch (exception& e) {
                cerr << e.what() << endl;
                throw logic_error("FOLLOW: error while filling FOLLOW set");
            }
        }
    } while(added);
}

void SlrGenerator::fill_slr_table() {

    map<std::string, unsigned int> term_numbers;
    map<std::string, unsigned int> non_term_numbers;

    unsigned int counter = 0;
    for (auto i_term = m_ast->get_term_begin(); i_term < m_ast->get_term_end(); ++i_term) {
        term_numbers[i_term->get_name()] = counter++;
    }
    counter = 0;
    for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
        non_term_numbers[i_non_term->get_name()] = counter++;
    }
    try {
        for (auto i : boost::irange(0, static_cast<int>(m_states.size()))) {
            vector<Field> action_line;
            vector<int> goto_line;

            for (auto i_term = m_ast->get_term_begin(); i_term < m_ast->get_term_end(); ++i_term) {
                action_line.push_back({err, 0, 0, 0});
            }

            for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
                goto_line.push_back(-1);
            }
            m_action.push_back(action_line);
            m_goto.push_back(goto_line);
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("Error while initializing ACTION and GOTO tables");
    }

    for (unsigned int i = 0; i < m_states.size(); ++i) {
        //action generator
        State i_state = m_states[i];

        for (auto i_item : i_state) {
            try {
                bool used = false;
                if (i_item.position < i_item.rule_body.size() and
                    i_item.rule_body[i_item.position].get_tag() != NNAME) {
                    auto term = i_item.rule_body[i_item.position];
                    auto new_state = find_in_states(slr_goto(&i_state, &term));
                    m_action[i][term_numbers[term.get_name()]] = {shift, static_cast<unsigned int>(new_state), 0, 0};
                    used = true;
                }
                if (i_item.position == i_item.rule_body.size() and not (i_item.rule_name == *m_axiom)) {
                    for (auto follow_term : m_follow[i_item.rule_name.get_name()]) {
                        m_action[i][term_numbers[follow_term->get_name()]] = {reduce, i,
                                                                              non_term_numbers[i_item.rule_name.get_name()],
                                                                              i_item.rule_body.size()};
                    }
                    if (used) {
                        throw logic_error("Conflict");
                    }
                    used = true;
                }

                if (i_item.rule_name == *m_axiom and i_item.position == 1) {
                    m_action[i][0].state = accept;
                    if (used) {
                        throw logic_error("Conflict");
                    }
                }
            } catch (logic_error& e) {
                throw logic_error("CONFLICT SHIFT/REDUCE");
            } catch (exception& e) {
                cerr << e.what() << endl;
                throw logic_error("Error while generating ACTION generator");
            }
        }
        try {
            for (auto i_non_term = m_ast->get_non_term_begin(); i_non_term < m_ast->get_non_term_end(); ++i_non_term) {
                int new_state = find_in_states(slr_goto(&i_state, &*i_non_term));
                if (new_state >= 0) {
                    m_goto[i][non_term_numbers[i_non_term->get_name()]] = new_state;
                }
            }
        } catch (exception& e) {
            cerr << e.what() << endl;
            throw logic_error("Error while generating GOTO generator");
        }
    }
}


Sequence SlrGenerator::find_first_for_seq(TermChain seq) {

    Sequence result_seq = {};
    bool eps_found = false;
    unsigned int i = 0;
    do {
        if (not m_first.count(seq[i].get_name())) {
            throw invalid_argument("FIRST for sequence: Symbol from rule body not found in FIRST set.");
        }

        for (auto first_term : m_first[seq[i].get_name()]) {
            if (first_term == m_epsilon) {
                eps_found = true;
                continue;
            }
            result_seq.insert(first_term);
        }

        if (i == seq.size() - 1 and eps_found) {
            result_seq.insert(m_epsilon);
        }
        ++i;


    } while (eps_found and i < seq.size());

    return result_seq;
}

int SlrGenerator::find_in_states(State state_to_be_checked) {
    unsigned int counter = 0;
    for (auto i_state : m_states) {
        if (state_to_be_checked == i_state)  {
            return counter;
        }
        ++counter;

    }
    return -1;
}


void SlrGenerator::printStates() {
    cout << "--------------------Printing states\n";
    int states_count = 0;
    for (auto i_state : m_states) {
        cout << "______State " << states_count++ << endl;
        for (auto& i_item : i_state) {
            cout << i_item.rule_name.get_name() << "->  \t";
            for (int i = 0; i < i_item.rule_body.size(); i++) {
                if (i == i_item.position) cout << ".";
                cout << i_item.rule_body[i].get_name() << " ";
            }
            if (i_item.position == i_item.rule_body.size()) cout << ".";
            cout << endl;
        }
    }
}

void SlrGenerator::printFirst() {
    cout << "--------------------Printing First Set\n";
    for (auto first : m_first) {
        cout << first.first << ":  ";
        for (auto term : first.second) {
            cout << term->get_name() << " ";
        }
        cout << endl;
    }
}

void SlrGenerator::printFollow() {
    cout << "--------------------Printing Follow Set\n";
    for (auto follow : m_follow) {
        cout << follow.first << ":  ";
        for (auto term : follow.second) {
            cout << term->get_name() << " ";
        }
        cout << endl;
    }
}

