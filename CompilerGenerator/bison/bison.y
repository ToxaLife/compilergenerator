%{
    #include <stdio.h>
    #include "../flex/flex.h"
    #include "../include/tag_names/FirstGrammarTags.hpp"
    #include "../symbol/Nterm.hpp"
    #include "../symbol/Term.hpp"
%}

%define api.pure

%locations
%lex-param {yyscan_t scanner}
%parse-param {yyscan_t scanner}
%parse-param {long env[26]}
%parse-param {Nterm* root}

%union {
    char* value;
}

%token NEW_LINE_TOKEN
%token <value>     NNAME_TOKEN
%token <value>      TNAME_TOKEN
%token EQUAL_TOKEN
%token AXIOM_DECL_TOKEN
%token NTERM_DECL_TOKEN
%token TERM_DECL_TOKEN
%token RULE_DECL_TOKEN
%token EPS_DECL_TOKEN

%type <nterm>   S A T N R Nlist Tlist Rrule RightPath Action Chain Sym NLlist NL

%type <term>    NEW_LINE NNAME TNAME EQUAL AXIOM_DECL NTERM_DECL TERM_DECL RULE_DECL EPS_DECL

%{
    void yyerror(YYLTYPE *loc, yyscan_t scanner, long env[26], Nterm* root, const char *msg);
    void yyerror(YYLTYPE *yylloc, yyscan_t scanner, long env[26], const char *msg);
%}

%%


S:          A NLlist N NLlist T NLlist R
            {
                root->add_symbol($1);
                root->add_symbol($2);
                root->add_symbol($3);
                root->add_symbol($4);
                root->add_symbol($5);
                root->add_symbol($6);
                root->add_symbol($7);
            };

A:          AXIOM_DECL NNAME
            {
                $$ = new Nterm(A);
                $$->add_symbol($1);
                $$->add_symbol($2);
            };

N:          NTERM_DECL Nlist
            {
                $$ = new Nterm(N);
                $$->add_symbol($1);
                $$->add_symbol($2);
            };

T:          TERM_DECL Tlist
            {
                $$ = new Nterm(T);
                $$->add_symbol($1);
                $$->add_symbol($2);

            };

R:          Rrule R
            {
               $$ = new Nterm(R);
               $$->add_symbol($1);
               $$->add_symbol($2);
            }
            |
            {
                $$ = new Nterm(R);
            };

Rrule:      RULE_DECL NNAME EQUAL RightPath
            {
                $$ = new Nterm(Rrule);
                $$->add_symbol($1);
                $$->add_symbol($2);
                $$->add_symbol($3);
                $$->add_symbol($4);
            };

RightPath:  Action NL RightPath
            {
                $$ = new Nterm(RightPath);
                $$->add_symbol($1);
                $$->add_symbol($2);
                $$->add_symbol($3);
            }
            | Action NLlist
            {
                $$ = new Nterm(RightPath);
                $$->add_symbol($1);
                $$->add_symbol($2);
            };

Action:     Chain
            {
                $$ = new Nterm(Action);
                $$->add_symbol($1);
            }
            | EPS_DECL
            {
                $$ = new Nterm(Action);
                $$->add_symbol($1);
            };

Chain:      Sym Chain
            {
                $$ = new Nterm(Chain);
                $$->add_symbol($1);
                $$->add_symbol($2);
            }
            | Sym
            {
                $$ = new Nterm(Chain);
                $$->add_symbol($1);
            };

Sym:        NNAME
            {
                $$ = new Nterm(Sym);
                $$->add_symbol($1);
            }
            | TNAME
            {
                $$ = new Nterm(Sym);
                $$->add_symbol($1);
            };

Nlist:      NNAME Nlist
            {
                $$ = new Nterm(Nlist);
                $$->add_symbol($1);
                $$->add_symbol($2);
            }
            | NNAME
            {
                $$ = new Nterm(Nlist);
                $$->add_symbol($1);
            };

Tlist:      TNAME Tlist
            {
                $$ = new Nterm(Tlist);
                $$->add_symbol($1);
                $$->add_symbol($2);
            }
            | TNAME
            {
                $$ = new Nterm(Tlist);
                $$->add_symbol($1);
            };

NLlist:     NL NLlist
            {
                $$ = new Nterm(NLlist);
                $$->add_symbol($1);
                $$->add_symbol($2);
            }
            | NL
            {
                $$ = new Nterm(NLlist);
                $$->add_symbol($1);
            };

NL:         NEW_LINE
            {
                $$ = new Nterm(NL);
                $$->add_symbol($1);
            };

NEW_LINE:   NEW_LINE_TOKEN
            {
                $$ = new Term(NEW_LINE, "");
            };

NNAME:      NNAME_TOKEN
            {
                $$ = new Term(NNAME, $1);
            };

TNAME:      TNAME_TOKEN
            {
                $$ = new Term(TNAME, $1);
            };

EQUAL:      EQUAL_TOKEN
            {
                $$ = new Term(EQUAL, "");
            };

AXIOM_DECL: AXIOM_DECL_TOKEN
            {
                $$ = new Term(AXIOM_DECL, "");
            };

NTERM_DECL: NTERM_DECL_TOKEN
            {
                $$ = new Term(NTERM_DECL, "");
            };

TERM_DECL:  TERM_DECL_TOKEN
            {
                $$ = new Term(TERM_DECL, "");
            };

RULE_DECL:  RULE_DECL_TOKEN
            {
                $$ = new Term(RULE_DECL, "");
            };

EPS_DECL:   EPS_DECL_TOKEN
            {
                $$ = new Term(EPS_DECL, "");
            };


%%



