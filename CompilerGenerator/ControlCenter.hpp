//
// Created by Александр Поповкин on 29/11/15.
//

#ifndef COMPILERGENERATOR_CONTROLCENTER_HPP
#define COMPILERGENERATOR_CONTROLCENTER_HPP

#include <iostream>

class ControlCenter
{
private:
    static ControlCenter *p_instance;

    ControlCenter() {}
    ControlCenter( const ControlCenter& );
    ControlCenter& operator=( ControlCenter& );
public:
    static ControlCenter *get_instance() {
        if(!p_instance)
            p_instance = new ControlCenter();
        return p_instance;
    }

    void MainFunc(char *grammar, std::string grammar_name, bool first);


};



#endif //COMPILERGENERATOR_CONTROLCENTER_HPP
