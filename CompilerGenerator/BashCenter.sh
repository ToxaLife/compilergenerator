#!/usr/bin/env bash

if [ $# -eq 0 ]
then
    echo "Zero iteration"
    ./flex/flex.sh
    ./bison/Bison.sh
    ./include/tags_include.sh FirstGrammar
    ./parser/tables_include.sh FirstGrammar
else
    echo "Grammar: $1"
    ./include/tags_include.sh $1
    ./parser/tables_include.sh $1
fi

