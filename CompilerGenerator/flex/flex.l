
%option reentrant noyywrap bison-bridge bison-locations
%option extra-type="struct Extra *"
%option nounput
%{

    #include <stdio.h>
    #include <stdlib.h>
    #include "flex.h"
    #include "../include/Token.hpp"
    #include "../include/tag_names/FirstGrammarTags.hpp"
    #include "../bison/bison.tab.hpp"
    bool prepare_for_bison;
    #define YY_USER_ACTION                                  \
    {                                                       \
    int i;                                                  \
    struct Extra *extra = yyextra;                          \
    if (! extra->continued) {                               \
    yylloc->first_line = extra->cur_line;                   \
    yylloc->first_column = extra->cur_column;               \
    }                                                       \
    extra->continued = 0;                                   \
    for (i = 0; i < yyleng; i++)                            \
    {                                                       \
    if (yytext[i] == '\n')                                  \
    {                                                       \
				extra->cur_line++;                          \
                extra->cur_column = 1;                      \
                }                                           \
                else                                        \
                extra->cur_column++;                        \
                }                                           \
                                                            \
                yylloc->last_line = extra->cur_line;        \
                yylloc->last_column = extra->cur_column;    \
                }

    void yyerror(YYLTYPE *loc, yyscan_t scanner, long env[26], Nterm* root, const char *msg)
    {
        printf("Error (%d,%d): %s\n", loc->first_line, loc->first_column, msg);
    }

%}

%%
[\t ]+

[\n]                {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = NEW_LINE;
                        return prepare_for_bison ? NEW_LINE_TOKEN : NEW_LINE;
                    }

[A-Z][A-Za-z0-9_]*  {
                        yylval->length = yyleng;
                        yylval->tag = NNAME;
                        yylval->value = yytext;
                        return prepare_for_bison ? NNAME_TOKEN : NNAME;
                    }

\"[^\n ]+\"         {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = TNAME;
                        return prepare_for_bison ? TNAME_TOKEN : TNAME;
                    }

\$AXIOM             {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = AXIOM_DECL;
                        return prepare_for_bison ? AXIOM_DECL_TOKEN : AXIOM_DECL;
                    }

\$NTERM             {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = NTERM_DECL;
                        return prepare_for_bison ? NTERM_DECL_TOKEN : NTERM_DECL;
                    }

\$TERM              {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = TERM_DECL;
                        return prepare_for_bison ? TERM_DECL_TOKEN : TERM_DECL;
                    }

\$RULE              {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = RULE_DECL;
                        return prepare_for_bison ? RULE_DECL_TOKEN : RULE_DECL;
                    }

\$EPS               {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = EPS_DECL;
                        return prepare_for_bison ? EPS_DECL_TOKEN : EPS_DECL;
                    }

\=                  {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = EQUAL;
                        return prepare_for_bison ? EQUAL_TOKEN : EQUAL;
                    }

<<EOF>>             {
                        yylval->length = yyleng;
                        yylval->value = yytext;
                        yylval->tag = 0;
                        return 0;
                    }
%%



void init_scanner(char *program, yyscan_t *scanner, struct Extra *extra, bool prepare_for_bison_flag)
{
    prepare_for_bison = prepare_for_bison_flag;
    extra->continued = 0;
    extra->cur_line = 1;
    extra->cur_column = 1;
    yylex_init(scanner);
    yylex_init_extra(extra, scanner);
    yy_scan_string(program, *scanner);
}

void destroy_scanner(yyscan_t scanner)
{
    yylex_destroy(scanner);
}
