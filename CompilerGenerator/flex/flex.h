

#ifndef _LEXER_H_INCLUDED
#define _LEXER_H_INCLUDED

#include "../include/Token.hpp"


#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;
#endif



struct Extra {
    int continued;
    int cur_line;
    int cur_column;
};


void init_scanner(char *program, yyscan_t *scanner, struct Extra *extra, bool prepare_for_bison);
void destroy_scanner(yyscan_t scanner);


/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED

# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif
int yylex(YYSTYPE * yylval_param, YYLTYPE * yylloc_param , yyscan_t yyscanner);

#endif

