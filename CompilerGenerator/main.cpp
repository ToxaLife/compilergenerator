
#include <iostream>
#include <fstream>
#include "ControlCenter.hpp"
#define GRAMMAR_FILEPATH "grammars/"
#define FIRST_GRAMMAR_NAME "FirstGrammar"

ControlCenter* ControlCenter::p_instance = 0;

using namespace std;

int main(int argc, const char *argv[]) {
    bool zero_iteration = true;
    string current_grammar = FIRST_GRAMMAR_NAME;
    string current_filepath = GRAMMAR_FILEPATH;
    char* buffer;
    if (argc == 2) {
        //non-zero iteration
        zero_iteration = false;
        current_grammar = argv[1];
        cout << "Current Grammar: " << current_grammar << endl;
    } else {
        cout << "Zero iteration\n";
    }

    //read file with grammar
    ifstream grammar_file(current_filepath + current_grammar, ifstream::binary);

    if (!grammar_file) {
        cout << "Cannot open Grammar file\n";
        return 1;
    }

    grammar_file.seekg(0, grammar_file.end);
    long file_length = grammar_file.tellg();
    grammar_file.seekg(0, grammar_file.beg);

    buffer = new char[file_length];
    grammar_file.read(buffer, file_length);

    grammar_file.close();

    cout << "Grammar file successfully opened and read\n";

    //create singleton
    ControlCenter *my_center = ControlCenter::get_instance();

    try {
        my_center->MainFunc(buffer, current_grammar, zero_iteration);

    } catch (exception& e) {
        cerr << e.what() << endl;
        cerr << "Finished with errors\n";
        return 1;
    }


    cout << "Successfully finished\n";

    delete buffer;
    return 0;
}
