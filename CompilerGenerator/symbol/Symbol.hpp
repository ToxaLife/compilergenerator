//
// Created by Александр Поповкин on 04/12/15.
//

#ifndef COMPILERGENERATOR_SYMBOL_HPP
#define COMPILERGENERATOR_SYMBOL_HPP

#include <iostream>

class Symbol {
public:
    virtual void to_string(unsigned short level) const = 0 ;
    virtual unsigned int get_tag() const = 0;
    virtual std::string get_name() const = 0;


    bool operator==(const Symbol& other) const {
        return other.get_tag() == m_tag;
    }
protected:
    unsigned int m_tag;
};


#endif //COMPILERGENERATOR_SYMBOL_HPP
