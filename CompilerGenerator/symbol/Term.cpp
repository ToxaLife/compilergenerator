//
// Created by Александр Поповкин on 04/12/15.
//

#include <iostream>
#include "Term.hpp"
#include "../include/Token.hpp"

using namespace std;

Term::Term(unsigned int term_number, string value) {
    m_tag = term_number;
    m_name = value;
}

void Term::to_string(unsigned short level) const {
    cout << "|";
    for (unsigned short i = 0; i <= level; ++i) {
        cout << "---|";
    }
    cout << TermDeclNames[m_tag] << ":" << m_name << endl;
}

