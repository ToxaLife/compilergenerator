//
// Created by Александр Поповкин on 04/12/15.
//

#ifndef COMPILERGENERATOR_NTERM_HPP
#define COMPILERGENERATOR_NTERM_HPP

#include <iostream>
#include <vector>
#include "Symbol.hpp"




class Nterm: public Symbol {
public:
    Nterm(unsigned int non_term_number);
    Nterm() {
        m_tag = 1;
    }

    void to_string(unsigned short level) const override ;
    void add_symbol(Symbol *sym);

    void reverse_children() {
        std::reverse(m_children.begin(), m_children.end());
    };

    unsigned int get_tag() const override { return m_tag; };
    std::string get_name() const override;


    Symbol* get_child(unsigned int index) const {
        if (index >= m_children.size()) {
            throw std::out_of_range("Nterm: out of range.");
        }
        return m_children[index];
    };

    unsigned long get_children_count() const {
        return m_children.size();
    }

private:
    typedef std::vector<Symbol*> SymbolChain;

    SymbolChain m_children;

};


#endif //COMPILERGENERATOR_NTERM_HPP
