//
// Created by Александр Поповкин on 04/12/15.
//

#ifndef COMPILERGENERATOR_TERM_HPP
#define COMPILERGENERATOR_TERM_HPP


#include "Symbol.hpp"
#include "../include/IncludeTags.hpp"
#include <string>


class Term: public Symbol {
public:

    Term(unsigned int term_number, std::string value);
    Term() {
        m_tag = 1;
        m_name = "";
    };

    void to_string(unsigned short level) const override;

    unsigned int get_tag() const override { return m_tag; } ;
    std::string get_name() const override { return m_name; };
    std::string get_value() const { return TermDeclNames[m_tag]; };

    bool operator<(const Term& other) const {
        if (this->get_tag() != other.get_tag()) {
            return this->get_tag() < other.get_tag();
        } else {
            return this->get_value() < other.get_value();
        }
    }

    bool operator==(const Term& other) const {
        return other.get_name() == m_name;
    }


private:

    std::string m_name;
};


#endif //COMPILERGENERATOR_TERM_HPP
