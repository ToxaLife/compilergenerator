//
// Created by Александр Поповкин on 04/12/15.
//

#include <iostream>
#include "Nterm.hpp"
#include "../include/Token.hpp"

using namespace std;

Nterm::Nterm(unsigned int non_term_number) {
    m_tag = non_term_number;
}

void Nterm::to_string(unsigned short level) const {
    cout << "|";
    for (unsigned short i = 0; i <= level; ++i) {
        cout << "---|";
    }
    cout << NtermDeclNames[m_tag] << endl;

    for (auto child : m_children) {
        child->to_string(level + 1);
    }
}

void Nterm::add_symbol(Symbol *sym) {
    m_children.push_back(sym);
}

std::string Nterm::get_name() const {
    return __1::basic_string<char, char_traits<char>, allocator<char>>();
}
