//
// Created by Alexander Popovkin on 29/01/16.
//

#include <iostream>
#include "AstTree.hpp"
#include <boost/range/irange.hpp>
#include "../include/tag_names/FirstGrammarTags.hpp"

using namespace std;

AstTree::AstTree(Nterm root) {
    Symbol *child;

    //AXIOM
    try {
        child = root.get_child(0);
        if (auto axiom_path = dynamic_cast<Nterm *>(child)) { ;
            child = axiom_path->get_child(1);
            if (auto axiom = dynamic_cast<Term *>(child)) {
                m_axiom = *axiom;
            } else {
                throw logic_error("Value not found.");
            }
        } else {
            throw logic_error(" Path not found.");
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("Bad CST tree: Axiom error found");
    }


    //NTERMS
    try {
        child = root.get_child(2);
        if (auto nterm_path = dynamic_cast<Nterm *>(child)) {

            do {
                child = nterm_path->get_child(1);
                if (auto nterm_elem = dynamic_cast<Nterm *>(child)) {
                    child = nterm_elem->get_child(0);
                    if (auto nterm_value = dynamic_cast<Term *>(child)) {
                        nterm_path = nterm_elem;
                        m_non_terminals.push_back(*nterm_value);
                    } else {
                        throw logic_error("Value not found.");
                    }
                } else {
                    throw logic_error("Element not found.");
                }
            } while (nterm_path->get_children_count() > 1);
        } else {
            throw logic_error("Path not found.");
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("Bad CST tree: NonTerminals error found");
    }


    //TERMS
    try {
        child = root.get_child(4);
        if (auto term_path = dynamic_cast<Nterm *>(child)) {
            do {
                child = term_path->get_child(1);
                if (auto term_elem = dynamic_cast<Nterm *>(child)) {
                    child = term_elem->get_child(0);
                    if (auto term_value = dynamic_cast<Term *>(child)) {
                        term_path = term_elem;
                        m_terminals.push_back(*term_value);
                    } else {
                        throw logic_error("Value not found.");
                    }
                } else {
                    throw logic_error("Element not found.");
                }
            } while (term_path->get_children_count() > 1);
        } else {
            throw logic_error("Path not found.");
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("Bad CST tree: Terminals error found");
    }


    //RULES
    try {
        child = root.get_child(6);
        if (auto rule_path = dynamic_cast<Nterm *>(child)) {

            while (rule_path->get_children_count() > 1) {
                child = rule_path->get_child(0);
                if (auto rule_elem = dynamic_cast<Nterm *>(child)) {
                    child = rule_elem->get_child(3);
                    if (auto rule_body = dynamic_cast<Nterm *>(child)) {
                        Rule rule_to_be_added = {};

                        Nterm *rule_action = rule_body;

                        while (rule_action->get_children_count() > 1 and rule_action->get_tag() != NLlist) {

                            TermChain alt_to_be_added = {};
                            child = rule_action->get_child(0);
                            fill_chain(child, &alt_to_be_added);

                            rule_to_be_added.push_back(alt_to_be_added);

                            child = rule_action->get_child(static_cast<unsigned int>(rule_action->get_children_count() - 1));
                            if (auto next_rule_action = dynamic_cast<Nterm *>(child)) {
                                rule_action = next_rule_action;
                            } else {
                                throw logic_error("Body not found.");
                            }

                        }
                        child = rule_elem->get_child(1);
                        if (auto rule_name = dynamic_cast<Term *>(child)) {
                            m_rules[rule_name->get_name()] = rule_to_be_added;
                        } else {
                            throw logic_error("Name not found.");
                        }
                        child = rule_path->get_child(1);
                        if (auto next_rule_path = dynamic_cast<Nterm *>(child)) {
                            rule_path = next_rule_path;
                        } else {
                            throw logic_error("Element not found.");
                        }

                    } else {
                        throw logic_error("Body not found.");
                    }
                } else {
                    throw logic_error("Element not found.");
                }
            }
        } else {
            throw logic_error("Path not found.");
        }
    } catch (exception& e) {
        cerr << e.what() << endl;
        throw logic_error("Bad CST tree: Rules error found");
    }
}

void AstTree::fill_chain(Symbol *cst_elem, TermChain* chain) {
    if (Nterm* nterm = dynamic_cast<Nterm*>(cst_elem)) {
        for (auto i : boost::irange(0, static_cast<int>(nterm->get_children_count()))) {
            fill_chain(nterm->get_child(i), chain);
        }
    } else if(Term* term = dynamic_cast<Term*>(cst_elem)) {
        if (term->get_tag() == EPS_DECL) {
            term = new Term(term->get_tag(), "$");
            m_epsilon = *term;
        }
        chain->push_back(*term);
    } else {
        throw invalid_argument("Bad CST tree: unexpected symbol in rule body.");
    }
}

bool AstTree::ExtendTree() {
    if (m_tree_extended) {
        return false;
    }
    try {
        m_non_terminals.insert(m_non_terminals.begin(), m_axiom);

        m_axiom = Term::Term(m_axiom.get_tag(), m_axiom.get_name() + "'");


        m_non_terminals.insert(m_non_terminals.begin(), m_axiom);

        m_rules[m_axiom.get_name()] = {{m_non_terminals[1]}};

        m_terminals.insert(m_terminals.begin(), m_epsilon);
    } catch (exception& e) {
        throw logic_error("Bad AST tree: error while extending tree.");
    }
    return true;
}

void AstTree::ToString() const {
    cout << "AXIOM " << m_axiom.get_name() << endl;

    cout << "NTERM ";
    for (auto i_nterm : m_non_terminals) {
        cout << i_nterm.get_name() << " ";
    }
    cout << endl;

    cout << "TERM ";
    for (Term i_term : m_terminals) {
        cout << i_term.get_name() << " ";
    }
    cout << endl;
    cout << "RULES:" << endl;
    for (auto i_rule : m_rules) {
        cout << i_rule.first << endl;
        for (auto i_sym_chain : i_rule.second) {
            cout << "\t";
            for (auto i_sym : i_sym_chain) {
                if (i_sym.get_tag() == NNAME) {
                    cout << "NTERM(" << i_sym.get_name() << ") ";
                } else {
                    cout << "TERM(" << i_sym.get_name() << ") ";
                }
            }
            cout << endl;
        }
    }
}

