//
// Created by Alexander Popovkin on 29/01/16.
//

#ifndef COMPILERGENERATOR_TREE_H
#define COMPILERGENERATOR_TREE_H

#include <string>
#include <vector>
#include <map>
#include "../symbol/Nterm.hpp"
#include "../symbol/Term.hpp"

typedef std::vector<Term> TermChain;
typedef std::vector<TermChain> Rule;


typedef TermChain::const_iterator TermIterator;

class AstTree {
public:

    AstTree(Nterm root);
    bool ExtendTree();
    void ToString() const ;

    const Term* get_axiom() const { return &m_axiom; }
    const Term* get_epsilon() const { return &m_epsilon; }

    TermIterator get_term_begin() const { return m_terminals.begin(); }
    TermIterator get_term_end() const { return m_terminals.end(); }

    TermIterator get_non_term_begin() const { return m_non_terminals.begin(); }
    TermIterator get_non_term_end() const { return m_non_terminals.end(); }

    Rule get_rule(std::string rule_name) const {
        if (m_rules.count(rule_name)) {
            return m_rules.at(rule_name);
        }
        throw std::invalid_argument("AST tree: requested rule not found");
    }

private:
    Term m_axiom;
    Term m_epsilon;
    TermChain m_non_terminals;
    TermChain m_terminals;
    std::map<std::string, Rule> m_rules;
    bool m_tree_extended;

    void fill_chain(Symbol *cst_elem, TermChain *chain);

};


#endif //COMPILERGENERATOR_TREE_H
