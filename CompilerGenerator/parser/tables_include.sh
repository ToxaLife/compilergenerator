#!/usr/bin/env bash

HEADER=./parser/IncludeTables.hpp
echo "#ifndef COMPILERGENERATOR_ALLTAGS_H" > $HEADER
echo "#define COMPILERGENERATOR_ALLTAGS_H" >> $HEADER
echo "" >> $HEADER

echo "#include \"../bin/slr_tables/$1.hpp\"" >> $HEADER
echo "" >> $HEADER
echo "#endif" >> $HEADER

echo "Header Table file generated"