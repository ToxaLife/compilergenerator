//
// Created by Alexander Popovkin on 05/02/16.
//

#ifndef COMPILERGENERATOR_SLRPARSER_H
#define COMPILERGENERATOR_SLRPARSER_H


#include <deque>
#include <stack>
#include "../include/Token.hpp"

class SlrParser {
public:
    SlrParser(std::vector<YYTOKEN> input);
    Nterm* Parse();

private:
    unsigned int m_input_pos;
    std::vector<YYTOKEN> m_input;
    std::stack<std::pair<unsigned int, Symbol*>> m_parser_stack;

    YYTOKEN next_token();

    std::stack<YYTOKEN> m_token_stack;

};

#endif //COMPILERGENERATOR_SLRPARSER_H
