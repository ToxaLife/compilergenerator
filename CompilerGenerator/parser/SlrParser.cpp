//
// Created by Alexander Popovkin on 05/02/16.
//
#include <boost/range/irange.hpp>

#include "../include/SlrTableStructure.hpp"
#include "SlrParser.hpp"
#include "IncludeTables.hpp"

using namespace std;

SlrParser::SlrParser(std::vector<YYTOKEN> input) {
    m_input = input;
    m_input_pos = 0;
    m_token_stack.push(input.back());
    m_parser_stack.push({0, nullptr});
}

Nterm* SlrParser::Parse() {
    YYTOKEN current_token = next_token();
    Nterm* nterm;
    Term* term;
    Nterm* root = nullptr;
    bool coninue_pars = true;
    char* token_value;

    while (coninue_pars) {
        try {
            auto current_state = m_parser_stack.top().first;
            auto current_field = action_table[current_state][current_token.value.tag];

            switch (current_field.state) {
                case shift:
                    term = new Term(static_cast<unsigned int>(current_token.value.tag), current_token.value.get_value());
                    assert(term);
                    m_parser_stack.push({current_field.state_number, term});
                    current_token = next_token();
                    break;
                case reduce:
                    nterm = new Nterm(current_field.reduce_rule);
                    assert(nterm);
                    for (auto i : boost::irange(0, static_cast<int>(current_field.rule_length))) {
                        if (m_parser_stack.top().second->get_tag() != 0) {
                            nterm->add_symbol(m_parser_stack.top().second);
                        }
                        m_parser_stack.pop();
                    }
                    nterm->reverse_children();
                    current_state = m_parser_stack.top().first;
                    if (goto_table[current_state][current_field.reduce_rule] < 1) {
                        throw invalid_argument("Negative GOTO value");
                    }
                    m_parser_stack.push({goto_table[current_state][current_field.reduce_rule], nterm});
                    break;
                case accept:
                    root = dynamic_cast<Nterm*>(m_parser_stack.top().second);
                    coninue_pars = false;
                    break;
                default:
                    throw logic_error("Error");
            }
        } catch (logic_error& e) {
            cout << "Error: " << current_token.coords.to_string() << endl;
            return nullptr;
        } catch (exception& e) {
            cerr << e.what() << endl;
            throw invalid_argument("Parse: unexpected error");
        }
    }

    return root;
}

YYTOKEN SlrParser::next_token() {
    if (m_input_pos == m_input.size() - 1) {
        return m_input[m_input_pos];
    } else {
        return m_input[m_input_pos++];
    }
}
